MACRO lb
	ld \1, (\3) | (\2 << 8)
ENDM

MACRO check_measure
	lb bc, \1, \2
	call CheckMeasure
ENDM
