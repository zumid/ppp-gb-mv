SECTION "hram", HRAM

hGBType:: db

; manual sync, i don't really want to hack
; the sound engine to keep track of beats/measures
hHardcodedPreSongDelay:: db
hHardcodedSongTick:: db
hHardcodedSongMeasure:: db

; pokered autobgtransfer
hSPTemp:: dw
hAutoBGTransferPortion:: db

; game modes
hGameMode:: db
hOldGameMode:: db

; zeroed out on vblank int.
hAskVBlank:: db

; screen glitchiness effect
hFuckUpScreenEnabled:: db
