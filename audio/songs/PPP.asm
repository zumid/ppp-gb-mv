INCLUDE "constants/devsoundx.inc"
INCLUDE "macros/devsoundx.inc"

SECTION "PiPiPi", ROMX

; Ken Ashcorp - PiPiPi

Song_PPP:
	db 5, 5
	dw .ch1
	dw .ch2
	dw .ch3
	dw .ch4

; 11111111111111111111111111111111111111111111111111

.ch1
	sound_call .ch1_00
	sound_call .ch1_01
	sound_call .ch1_01
	sound_call .ch1_01
	sound_call .ch1_01
;-----
	sound_call .ch1_03
	sound_call .ch1_02
	sound_call .ch1_03
	sound_call .ch1_03
;-----
	sound_call .ch1_01
	sound_call .ch1_04
;-----
	sound_call .ch1_05
	sound_call .ch1_06
	sound_call .ch1_05
	sound_call .ch1_06
	sound_call .ch1_05
	sound_call .ch1_01
	sound_call .ch1_07
	sound_end

.ch1_07
	sound_instrument ins_ppp_08
	note C#, 2, 2
	sound_loop 12, .ch1_07
	sound_instrument ins_ppp_09
	note G_, 2, 2
	sound_instrument ins_ppp_08
	note C#, 2, 2
	sound_instrument ins_ppp_09
	note G_, 2, 2
	sound_instrument ins_ppp_08
	note C#, 2, 2
	sound_instrument ins_ppp_09
	note C#, 2, 2
	sound_instrument ins_ppp_08
	note C#, 2, 2
	sound_instrument ins_ppp_09
	note C#, 2, 2
	sound_instrument ins_ppp_08
	note C#, 2, 2
	sound_instrument ins_ppp_09
	note C#, 2, 2
	sound_instrument ins_ppp_08
	note C#, 2, 2
	note C#, 2, 2
	note C#, 2, 2
	rest 4
	sound_ret


.ch1_06
.ch1_06_1
	sound_instrument ins_ppp_08
	note C#, 2, 2
	sound_instrument ins_ppp_13
	note E_, 4, 2
	sound_loop 3, .ch1_06_1
.ch1_06_2
	sound_instrument ins_ppp_08
	note C#, 2, 2
	sound_instrument ins_ppp_13
	note F#, 4, 2
	sound_loop 3, .ch1_06_2
.ch1_06_3
	sound_instrument ins_ppp_08
	note C#, 2, 2
	sound_instrument ins_ppp_13
	note G#, 4, 2
	sound_loop 2, .ch1_06_3
	sound_instrument ins_ppp_08
	note C#, 2, 2
	note C#, 2, 1
	note C#, 2, 1
	sound_instrument ins_ppp_13
	note A_, 4, 2
	note A_, 4, 2
	rest 4
	note B_, 4, 2
	note B_, 4, 2
	rest 4
	sound_ret

.ch1_05
.ch1_05_1
	sound_instrument ins_ppp_08
	note C#, 2, 2
	sound_instrument ins_ppp_13
	note A_, 3, 2
	sound_loop 7, .ch1_05_1
.ch1_05_2
	sound_instrument ins_ppp_08
	note C#, 2, 2
	sound_instrument ins_ppp_13
	note C_, 4, 2
	sound_loop 3, .ch1_05_2
.ch1_05_3
	sound_instrument ins_ppp_08
	note C#, 2, 2
	sound_instrument ins_ppp_13
	note D#, 4, 2
	sound_loop 3, .ch1_05_3
	sound_ret

.ch1_04
	sound_instrument ins_ppp_08
	note C#, 2, 2
	sound_instrument ins_ppp_09
	note C#, 2, 2
	sound_loop 5, .ch1_04
	sound_instrument ins_ppp_08
	note C_, 2, 2
	sound_instrument ins_ppp_09
	note G_, 2, 2
	sound_instrument ins_ppp_08
	note C_, 2, 2
	sound_instrument ins_ppp_09
	note G_, 2, 2
.ch1_04_1
	sound_instrument ins_ppp_08
	note C#, 2, 2
	sound_instrument ins_ppp_09
	note C#, 2, 2
	sound_loop 3, .ch1_04_1
	sound_instrument ins_ppp_08
	note C_, 2, 2
	rest 14
	sound_ret

.ch1_03
	sound_instrument ins_ppp_08
.ch1_03_1
	note C#, 2, 2
	rest 2
	sound_loop 15, .ch1_03_1
	sound_ret

.ch1_02
	sound_instrument ins_ppp_08
.ch1_02_1
	note C#, 2, 2
	rest 2
	sound_loop 8, .ch1_02_1
	rest 4
	note C#, 2, 2
	rest 6
	note C#, 2, 2
	rest 6
	note C#, 2, 2
	rest 2
	note C#, 2, 2
	rest 2
	sound_ret

.ch1_01
	sound_instrument ins_ppp_08
	note C#, 2, 2
	sound_instrument ins_ppp_09
	note C#, 2, 2
	sound_loop 5, .ch1_01
	sound_instrument ins_ppp_08
	note C_, 2, 2
	sound_instrument ins_ppp_09
	note G_, 2, 2
	sound_instrument ins_ppp_08
	note C_, 2, 2
	sound_instrument ins_ppp_09
	note G_, 2, 2
.ch1_01_1
	sound_instrument ins_ppp_08
	note C#, 2, 2
	sound_instrument ins_ppp_09
	note C#, 2, 2
	sound_loop 5, .ch1_01_1
	sound_instrument ins_ppp_08
	note C_, 2, 2
	sound_instrument ins_ppp_09
	note G_, 2, 2
	sound_instrument ins_ppp_08
	note C_, 2, 2
	note C_, 2, 2
	sound_ret

.ch1_00
	rest 58
	sound_instrument ins_ppp_0a
	note C_, 4, 2
	rest 2
	note C_, 4, 2
	sound_ret

; 22222222222222222222222222222222222222222222

.ch2
	sound_call .ch2_00
	sound_call .ch2_01
	sound_call .ch2_01
	sound_call .ch2_01
	sound_call .ch2_01
;-------
	sound_call .ch2_02
	sound_call .ch2_03
	sound_call .ch2_04
	sound_call .ch2_05
;-----
	sound_call .ch2_06
	sound_call .ch2_07
;-----
	sound_instrument ins_ppp_12
	sound_call .ch2_080a
	sound_call .ch2_090b
	sound_instrument ins_ppp_0e
	sound_call .ch2_080a
	sound_call .ch2_090b
	sound_call .ch2_0c
	sound_call .ch2_0e
	sound_call .ch2_0d
	sound_end

.ch2_0d
	sound_instrument ins_ppp_03
	note C#, 4, 4
	note C#, 4, 4
	note C#, 4, 4
	note C#, 4, 4
	note C#, 4, 4
	note C#, 4, 4
	sound_slide_down 15
	release 2
	sound_instrument ins_ppp_0e
	note G_, 5, 2
	rest 2
	note G_, 5, 2
	sound_instrument ins_ppp_03
	note C#, 4, 4
	note C#, 4, 4
	note C#, 4, 4
	note C#, 4, 4
	note C#, 4, 2
	rest 2
	sound_ret

.ch2_0e
	sound_instrument ins_ppp_03
	note C#, 4, 4
	note C#, 4, 4
	note C#, 4, 4
	note C#, 4, 4
	note C#, 4, 4
	note C#, 4, 4
	sound_slide_down 15
	release 2
	sound_instrument ins_ppp_0e
	note G_, 4, 2
	rest 2
	note G_, 4, 2
	sound_instrument ins_ppp_03
	note C#, 4, 4
	note C#, 4, 4
	note C#, 4, 4
	note C#, 4, 4
	note C#, 4, 4
	note C#, 4, 4
	sound_slide_down 15
	release 2
	sound_instrument ins_ppp_0e
	note G_, 4, 2
	rest 2
	note G_, 4, 2
	sound_ret

.ch2_0c
	sound_instrument ins_ppp_0e
	note E_, 5, 2
	sound_slide_up 3
	wait 2
	note F#, 5, 4
	note F#, 5, 2
	sound_slide_down 3
	wait 2
	note E_, 5, 4
	sound_loop 2, .ch2_0c
	note D#, 5, 4
	note D#, 5, 4
	note D#, 5, 4
	note E_, 5, 2
	sound_slide_down 4
	wait 2
	sound_ret

.ch2_080a
	note E_, 5, 2
	sound_slide_up 3
	wait 2
	note F#, 5, 4
	note F#, 5, 2
	sound_slide_down 3
	wait 2
	note E_, 5, 4
	sound_loop 1, .ch2_080a
	note E_, 5, 2
	sound_slide_up 3
	wait 2
	note F#, 5, 4
	note F#, 5, 2
	sound_slide_up 3
	wait 2
	note G#, 5, 2
	sound_slide_up 3
	wait 2
	note F#, 5, 2
	sound_slide_down 3
	wait 2
	release 4
	note E_, 5, 2
	release 2
	note E_, 5, 4
	sound_ret

.ch2_090b
	note E_, 5, 2
	sound_slide_up 3
	wait 2
	note F#, 5, 4
	note F#, 5, 2
	sound_slide_down 3
	wait 2
	note E_, 5, 4
	sound_loop 2, .ch2_090b
	sound_instrument ins_ppp_08
	note C#, 2, 2
	note C#, 2, 2
	rest 4
	note C#, 2, 2
	note C#, 2, 2
	rest 4
	sound_ret

.ch2_07
	sound_slide_up 3
	wait 48
	sound_toggle_monty
	rest 16
	sound_ret

.ch2_06
	sound_instrument ins_ppp_11
	note E_, 5, 1
	sound_slide_down 2
	sound_toggle_monty
	wait 63
	sound_ret

.ch2_05
	sound_instrument ins_ppp_09
	note G#, 3, 4
	note G#, 3, 4
	sound_instrument ins_ppp_0f
	note E_, 5, 2
	sound_slide_down 3
	wait 2
	note C#, 5, 4
	note E_, 5, 4
	sound_portamento 8
	note F#, 5, 2
	sound_instrument ins_ppp_10
	note F#, 5, 0
	sound_slide_down 5
	wait 2
	sound_instrument ins_ppp_0f
	note F#, 5, 2
	sound_slide_down 5
	wait 2
	note E_, 5, 4
	sound_instrument ins_ppp_0e
	note G#, 5, 8
	note C#, 6, 8
	note F#, 5, 6
	note F#, 5, 6
	note E_, 5, 4
	sound_ret

.ch2_04
	sound_instrument ins_ppp_09
	note G#, 3, 4
	note G#, 3, 4
	sound_instrument ins_ppp_0f
	note E_, 5, 4
	note E_, 5, 2
	sound_slide_down 2
	wait 2
	sound_instrument ins_ppp_09
	note G#, 3, 4
	note G#, 3, 4
	sound_instrument ins_ppp_0f
	note F#, 5, 2
	sound_slide_down 4
	wait 2
	note E_, 5, 4
	sound_instrument ins_ppp_09
	note E_, 3, 4
	note E_, 3, 4
	sound_instrument ins_ppp_0f
	note E_, 5, 4
	note C#, 5, 2
	sound_slide_down 5
	wait 2
	note E_, 5, 4
	sound_portamento 8
	note F#, 5, 2
	sound_instrument ins_ppp_10
	note C#, 5, 2
	sound_instrument ins_ppp_0f
	note F#, 5, 2
	sound_slide_down 6
	wait 2
	note E_, 5, 4
	sound_ret

.ch2_03
	sound_instrument ins_ppp_09
	note G#, 3, 4
	note G#, 3, 4
	sound_instrument ins_ppp_0e
	note C#, 4, 4
	note D#, 4, 4
	note E_, 4, 4
	note D#, 4, 4
	note C#, 4, 4
	note C_, 4, 4
	note E_, 4, 4
	rest 4
	note E_, 4, 4
	rest 4
	note D#, 4, 4
	sound_slide_down 4
	wait 4
	note C_, 4, 4
	note C#, 4, 4
	sound_ret

.ch2_02
	sound_instrument ins_ppp_09
	note G#, 3, 4
	note G#, 3, 4
	sound_instrument ins_ppp_0d
	note E_, 4, 4
	note E_, 4, 4
	sound_instrument ins_ppp_09
	note G#, 3, 4
	note G#, 3, 2
	sound_instrument ins_ppp_0d
	note C#, 4, 2
	note F#, 4, 4
	note E_, 4, 4
	sound_instrument ins_ppp_09
	note E_, 3, 4
	note E_, 3, 2
	sound_instrument ins_ppp_0d
	note C#, 4, 2
	note E_, 4, 4
	note E_, 4, 4
	sound_instrument ins_ppp_09
	note D#, 3, 4
	note D#, 3, 2
	sound_instrument ins_ppp_0d
	note C#, 4, 2
	note F#, 4, 4
	note E_, 4, 4
	sound_ret

.ch2_01
	sound_instrument ins_ppp_03
	note C#, 4, 4
	note C#, 4, 4
	note C#, 4, 4
	note C#, 4, 4
	note C#, 4, 4
	note C#, 4, 4
	sound_slide_down 15
	release 2
	note G_, 4, 2
	rest 2
	note G_, 4, 2
	note C#, 4, 4
	note C#, 4, 4
	note C#, 4, 4
	note C#, 4, 4
	note C#, 4, 4
	note C#, 4, 4
	sound_slide_down 15
	release 2
	note G_, 4, 2
	rest 2
	note G_, 4, 2
	sound_ret

.ch2_00
	sound_instrument ins_ppp_00
	note C#, 3, 4
	note C#, 3, 4
	note C#, 3, 4
	note C#, 3, 4
	note C#, 3, 4
	note C#, 3, 4
	rest 2
	note G_, 3, 2
	rest 2
	note G_, 3, 2
	note C#, 3, 4
	note C#, 3, 4
	note C#, 3, 4
	note C#, 3, 4
	note C#, 3, 4
	note C#, 3, 4
	rest 2
	sound_instrument ins_ppp_0a
	note G_, 3, 2
	rest 2
	note G_, 3, 2
	sound_ret

; 333333333333333333333333333333333333333333333

.ch3
	rest 64
	sound_call .ch3_01
	sound_call .ch3_01
	sound_call .ch3_01
	sound_call .ch3_01
; ---------
	sound_call .ch3_02
	sound_call .ch3_03
	sound_call .ch3_02
	sound_call .ch3_02
;-----
	sound_call .ch3_01
	sound_call .ch3_04
;-----
	sound_call .ch3_05
	sound_call .ch3_06
	sound_call .ch3_05
	sound_call .ch3_07
	sound_call .ch3_05
	sound_call .ch3_01
	sound_call .ch3_08
	sound_end

.ch3_08
	sound_instrument ins_ppp_01
	note C#, 2, 4
	note C#, 2, 4
	note C#, 2, 4
	note C#, 2, 4
	note C#, 2, 4
	note C#, 2, 4
	note C#, 2, 0
	sound_slide_down 15
	wait 2
	sound_instrument ins_ppp_02
	note G_, 2, 4
	note G_, 2, 2
	sound_instrument ins_ppp_01
	note C#, 2, 4
	note C#, 2, 4
	note C#, 2, 4
	note C#, 2, 4
	sound_instrument ins_ppp_02
	note C#, 2, 2
	sound_slide_down 5
	wait 2
	rest 4
	sound_ret

.ch3_07
	sound_instrument ins_ppp_01
	note C#, 2, 4
	note C#, 2, 4
	note C#, 2, 4
	note C#, 2, 4
	note D#, 2, 4
	note D#, 2, 4
	note D#, 2, 4
	note D#, 2, 4
	note E_, 2, 4
	note E_, 2, 4
	note E_, 2, 4
	note E_, 2, 4
	note F#, 2, 4
	note F#, 2, 4
	note G_, 2, 4
	note G_, 2, 4
	sound_ret

.ch3_06
	sound_instrument ins_ppp_01
	note C#, 2, 4
	note C#, 2, 4
	note C#, 2, 4
	note C#, 2, 4
	note D#, 2, 4
	note D#, 2, 4
	note D#, 2, 4
	note D#, 2, 4
	note E_, 2, 4
	note E_, 2, 4
	note E_, 2, 4
	note E_, 2, 4
	note F#, 2, 4
	rest 4
	note G_, 2, 4
	note G_, 2, 4
	sound_ret
	
.ch3_05
	sound_instrument ins_ppp_01
	note A_, 2, 4
	note A_, 2, 4
	note A_, 2, 4
	note A_, 2, 4
	note A_, 2, 4
	note A_, 2, 4
	note A_, 2, 4
	note A_, 2, 4
	note G#, 2, 4
	note G#, 2, 4
	note G#, 2, 4
	note G#, 2, 4
	note C_, 2, 4
	note C_, 2, 4
	note C_, 2, 4
	note C_, 2, 4
	sound_ret

.ch3_04
	sound_instrument ins_ppp_01
	note C#, 2, 4
	note C#, 2, 4
	note C#, 2, 4
	note C#, 2, 4
	note C#, 2, 4
	note C#, 2, 4
	note C#, 2, 2
	sound_instrument ins_ppp_02
	note G_, 2, 4
	note G_, 2, 2
	sound_instrument ins_ppp_01
	note C#, 2, 4
	note C#, 2, 4
	note C#, 2, 4
	note C#, 2, 4
	sound_instrument ins_ppp_02
	note C#, 2, 4
	rest 12
	sound_ret

.ch3_03
	sound_instrument ins_ppp_01
	note C#, 2, 4
	note C#, 2, 4
	note C#, 2, 4
	note C#, 2, 4
	note C_, 2, 4
	note C_, 2, 4
	note C_, 2, 4
	note C_, 2, 4
	note A_, 2, 4
	rest 4
	note A_, 2, 4
	rest 4
	note G#, 2, 4
	rest 4
	note G#, 2, 2
	rest 2
	note G#, 2, 4
	sound_ret

.ch3_02
	sound_instrument ins_ppp_01
	note C#, 2, 4
	note C#, 2, 4
	note C#, 2, 4
	note C#, 2, 4
	note C_, 2, 4
	note C_, 2, 4
	note C_, 2, 4
	note C_, 2, 4
	note A_, 2, 4
	note A_, 2, 4
	note A_, 2, 4
	note A_, 2, 4
	note G#, 2, 4
	note G#, 2, 4
	note G#, 2, 4
	note G#, 2, 4
	sound_ret

.ch3_01
	sound_instrument ins_ppp_01
	note C#, 2, 4
	note C#, 2, 4
	note C#, 2, 4
	note C#, 2, 4
	note C#, 2, 4
	note C#, 2, 4
	note C#, 2, 0
	sound_slide_down 15
	wait 2
	sound_instrument ins_ppp_02
	note G_, 2, 4
	note G_, 2, 2
	sound_instrument ins_ppp_01
	note C#, 2, 4
	note C#, 2, 4
	note C#, 2, 4
	note C#, 2, 4
	note C#, 2, 4
	note C#, 2, 4
	note C#, 2, 0
	sound_slide_down 15
	wait 2
	sound_instrument ins_ppp_02
	note G_, 2, 4
	note G_, 2, 2
	sound_ret

; 444444444444444444444444444444444444444444444
	
.ch4
	sound_call .ch4_00
	sound_call .ch4_01
	sound_call .ch4_02
	sound_call .ch4_01
	sound_call .ch4_02
; ----------
	sound_call .ch4_03
	sound_call .ch4_04
	sound_call .ch4_01
	sound_call .ch4_02
;-----
	sound_call .ch4_01
	sound_call .ch4_05
;-----
	sound_call .ch4_01
	sound_call .ch4_06
	sound_call .ch4_01
	sound_call .ch4_07
	sound_call .ch4_01
	sound_call .ch4_08
	sound_call .ch4_09
	sound_end

.ch4_09
	sfixins ins_ppp_0c, 2
	sfixins ins_ppp_07, 1
	sfixins ins_ppp_07, 1
	sfixins ins_ppp_0c, 2
	sfixins ins_ppp_0c, 1
	sfixins ins_ppp_07, 1
	sound_loop 2, .ch4_09
	sfixins ins_ppp_05, 2
	sfixins ins_ppp_0c, 2
	sfixins ins_ppp_07, 2
	sfixins ins_ppp_0c, 2
	sfixins ins_ppp_0c, 4
	sfixins ins_ppp_07, 1
	sfixins ins_ppp_07, 1
	sfixins ins_ppp_05, 2
	sfixins ins_ppp_07, 2
	sfixins ins_ppp_0c, 2
	sfixins ins_ppp_06, 2
	sfixins ins_ppp_07, 2
	sfixins ins_ppp_0c, 4
	rest 5
	sound_ret

.ch4_08
	sfixins ins_ppp_0c, 4
	sfixins ins_ppp_07, 2
	sfixins ins_ppp_06, 2
	sfixins ins_ppp_05, 2
	sfixins ins_ppp_06, 2
	sfixins ins_ppp_07, 2
	sfixins ins_ppp_06, 2
	sfixins ins_ppp_05, 2
	sfixins ins_ppp_06, 2
	sfixins ins_ppp_07, 2
	sfixins ins_ppp_06, 2
	sfixins ins_ppp_05, 2
	sfixins ins_ppp_0c, 2
	sfixins ins_ppp_07, 2
	sfixins ins_ppp_0c, 2
	sound_loop 1, .ch4_08
	sound_ret

.ch4_07
	sfixins ins_ppp_05, 2
	sfixins ins_ppp_06, 2
	sfixins ins_ppp_07, 2
	sfixins ins_ppp_06, 2
	sound_loop 4, .ch4_06
	sfixins ins_ppp_05, 2
	sfixins ins_ppp_06, 2
	sfixins ins_ppp_07, 2
	sfixins ins_ppp_05, 1
	sfixins ins_ppp_05, 1
	sfixins ins_ppp_07, 2
	sfixins ins_ppp_07, 2
	sfixins ins_ppp_06, 1
	sfixins ins_ppp_06, 1
	sfixins ins_ppp_06, 1
	sfixins ins_ppp_06, 1
	sfixins ins_ppp_07, 2
	sfixins ins_ppp_07, 2
	sfixins ins_ppp_07, 1
	sfixins ins_ppp_07, 1
	sfixins ins_ppp_07, 1
	sfixins ins_ppp_07, 1
	sound_ret

.ch4_06
	sfixins ins_ppp_05, 2
	sfixins ins_ppp_06, 2
	sfixins ins_ppp_07, 2
	sfixins ins_ppp_06, 2
	sound_loop 4, .ch4_06
	sfixins ins_ppp_05, 2
	sfixins ins_ppp_06, 2
	sfixins ins_ppp_07, 2
	sfixins ins_ppp_05, 1
	sfixins ins_ppp_05, 1
	sfixins ins_ppp_07, 2
	sfixins ins_ppp_07, 2
	rest 4
	sfixins ins_ppp_07, 2
	sfixins ins_ppp_07, 2
	sfixins ins_ppp_04, 2
	sfixins ins_ppp_0b, 2
	sound_ret

.ch4_05
	sfixins ins_ppp_05, 2
	sfixins ins_ppp_06, 2
	sfixins ins_ppp_07, 2
	sfixins ins_ppp_06, 2
	sound_loop 3, .ch4_05
	sfixins ins_ppp_05, 1
	sfixins ins_ppp_06, 1
	sfixins ins_ppp_06, 1
	sfixins ins_ppp_06, 1
	sfixins ins_ppp_07, 1
	sfixins ins_ppp_06, 1
	sfixins ins_ppp_06, 1
	sfixins ins_ppp_06, 1
	sfixins ins_ppp_05, 1
	sfixins ins_ppp_06, 1
	sfixins ins_ppp_06, 1
	sfixins ins_ppp_06, 1
	sfixins ins_ppp_07, 1
	sfixins ins_ppp_06, 1
	sfixins ins_ppp_05, 1
	sfixins ins_ppp_06, 1
	sfixins ins_ppp_07, 4
	sfixins ins_ppp_04, 8
	sfixins ins_ppp_04, 2
	sfixins ins_ppp_0b, 2
	sound_ret

.ch4_04
	sfixins ins_ppp_05, 4
	sound_loop 7, .ch4_04
.ch4_04_1
	sfix 2
	sfix 2
	rest 4
	sound_loop 2, .ch4_04_1
	sfixins ins_ppp_07, 2
	sfixins ins_ppp_05, 2
	sfixins ins_ppp_07, 2
	sfixins ins_ppp_07, 1
	sfix 1
	sound_ret

.ch4_03
	sfixins ins_ppp_0c, 4
.ch4_03_1
	sfixins ins_ppp_05, 4
	sound_loop 14, .ch4_03_1
	sound_ret

.ch4_00
	rest 36
	sound_instrument ins_ppp_04
	sfix 8
	sfix 8
	sfix 8
	sfixins ins_ppp_0b, 4
	sound_ret

.ch4_01
	sfixins ins_ppp_0c, 4
	sfixins ins_ppp_07, 2
	sfixins ins_ppp_06, 2
.ch4_01_1
	sfixins ins_ppp_05, 2
	sfixins ins_ppp_06, 2
	sfixins ins_ppp_07, 2
	sfixins ins_ppp_06, 2
	sound_loop 6, .ch4_01_1
	sound_ret

.ch4_02
	sfixins ins_ppp_05, 2
	sfixins ins_ppp_06, 2
	sfixins ins_ppp_07, 2
	sfixins ins_ppp_06, 2
	sound_loop 5, .ch4_02
.ch4_02_1
	sfixins ins_ppp_07, 1
	sfixins ins_ppp_06, 1
	sfixins ins_ppp_06, 1
	sfixins ins_ppp_06, 1
	sound_loop 1, .ch4_02_1
.ch4_02_2
	sfixins ins_ppp_05, 1
	sfixins ins_ppp_06, 1
	sfixins ins_ppp_07, 1
	sfixins ins_ppp_06, 1
	sfixins ins_ppp_05, 1
	sfixins ins_ppp_05, 1
	sfixins ins_ppp_07, 1
	sfixins ins_ppp_07, 1
	sound_ret
	

; --------- instruments ------------------

ins_ppp_12:
	dw  .vol, .arp, .duty, .pitch
	dw .volr,    0,     0,      0
.vol:   db 15, 12, 12, 12, 11, 11, 10, 10, 9, seq_end
.arp:   db 12, 0, seq_end
.duty:  db 0, seq_end
.pitch: db pitch_end
.volr:  db 5, seq_end

ins_ppp_13:
	dw   .vol, .arp, .duty, .pitch
	dw      0,    0,     0,      0
.vol:   db 2,3,4,5,6,7,7,8,9,10,11,seq_end
.arp:   db seq_end
.duty:  db 1, 1, 0, 0, seq_loop, -5
.pitch: db pitch_end

ins_ppp_11:
	dw .vol, .arp, .duty, .pitch
	dw    0,    0,     0,      0
.vol:   db 2, 2, 2, 3, 3, 3, 3, 3, 4, 5, 6, 7, 7, 7, 8, 9, 9, 9, 10, 9, seq_loop, -22
.arp:   db seq_end
.duty:  db 0, 0, 1, 1, seq_end
.pitch: db pitch_end

ins_ppp_10:
	dw .vol, .arp, .duty, .pitch
	dw    0,    0,     0,      0
.vol:   db 5,seq_end
.arp:   db seq_end
.duty:  db 1, seq_end
.pitch: db pitch_end

ins_ppp_0f:
	dw .vol, .arp, .duty, .pitch
	dw    0,    0,     0,      0
.vol:   db 15, 12, 12, 12, 11, 11, 10, 10, 9, seq_end
.arp:   db 12, 0, seq_end
.duty:  db 1, seq_end
.pitch: db pitch_end

ins_ppp_0e:
	dw .vol, .arp, .duty, .pitch
	dw    0,    0,     0,      0
.vol:   db 15, 14, 13, 13, 13, 12, 12, 12, 12, 11, 11, 10, 10, 10, 9, 9, 8, 8, 7, 6, 5,seq_end
.arp:   db 12, 0, 0, 7, 0, seq_loop, -6, seq_end
.duty:  db 1, 1, 1, 1, 1, 2, 1, seq_end
.pitch: db pitch_end

ins_ppp_0d:
	dw .vol, .arp, .duty, .pitch
	dw    0,    0,     0,      0
.vol:   db 15,14,13,13,13,12,12,12,11,11,10,10,9,8,8,7,7,7,6,seq_end
.arp:   db 19,0, seq_end
.duty:  db 2, seq_end
.pitch: db pitch_end

ins_ppp_04: ; stcik
	dw .vol, .arp, .duty, .pitch
	dw    0,    0,     0,      0
.vol:   db 13,9,5,4,3,3,2,2,2,2,2,2,1,1,1,1,1,0, seq_end
.arp:   db 40, seq_end
.duty:  db 1, seq_end
.pitch: db pitch_end

ins_ppp_0b: ; reverse cymbal
	dw .vol, .arp, .duty, .pitch
	dw    0,    0,     0,      0
.vol:   db 1,1,2,2,3,3,4,4,5,5,6,6,7,7,8,8,9,9,10, seq_end
.arp:   db 35, seq_end
.duty:  db 0, seq_end
.pitch: db pitch_end

ins_ppp_05: ; kick
	dw .vol, .arp, .duty, .pitch
	dw    0,    0,     0,      0
.vol:   db 15,15,15,15,5,2,1,1,1,0, seq_end
.arp:   db 33, 20, 11, seq_end
.duty:  db 0, seq_end
.pitch: db pitch_end

ins_ppp_06: ; double hat
	dw .vol, .arp, .duty, .pitch
	dw    0,    0,     0,      0
.vol:   db 8,5,3,1,0,3,2, seq_end
.arp:   db 33, seq_end
.duty:  db 0, seq_end
.pitch: db pitch_end

ins_ppp_07: ; snare
	dw .vol, .arp, .duty, .pitch
	dw    0,    0,     0,      0
.vol:   db 12, 11, 10, 10, 9, 9, 8, 8, 7, 5, 4, 3, 1, 1, 1, 0, seq_end
.arp:   db 25, 27, 32, seq_end
.duty:  db 1, 1, 0, seq_end
.pitch: db pitch_end

ins_ppp_0c: ; cymbal
	dw .vol, .arp, .duty, .pitch
	dw    0,    0,     0,      0
.vol:   db 12, 11, 10, 10, 9, 8, 8, 7, 7, 7, 6, 6, 5, seq_end
.arp:   db 28, 31, 35, seq_end
.duty:  db 0, seq_end
.pitch: db pitch_end

ins_ppp_00:
	dw .vol, .arp, .duty, .pitch
	dw    0,    0,     0,      0
.vol:   db 13, 7, 10, 13, 10, 9, 8, 7, 6, 6, 13, 10, 9, 8, 8, 7, seq_end
.arp:   db 1, 1, 0, seq_end
.duty:  db 2, 2, 1, seq_end
.pitch: db 0, 5, pitch_end

ins_ppp_01:
	dw .vol, .arp, .duty, .pitch
	dw    0,    0,     0,      0
.vol:   db $40, seq_wait, 8, $20, seq_end
.arp:   db 0, 0, 0, 12, 12, 12, 12, 12, 0, 0, 0, seq_loop, -12, seq_end
.duty:  db 20, seq_end
.pitch: db pitch_end

ins_ppp_02:
	dw .vol, .arp, .duty, .pitch
	dw    0,    0,     0,      0
.vol:   db $20, seq_wait, 8, $40, seq_end
.arp:   db seq_end
.duty:  db 20, seq_end
.pitch: db pitch_end

ins_ppp_0a:
	dw .vol, .arp, .duty, .pitch
	dw    0,    0,     0,      0
.vol:   db 13, 7, 10, 12, 11, 10, 8, 8, 6, seq_end
.arp:   db 64+12, 64+12, 0, 0, seq_loop, -5
.duty:  db 2, 1, seq_end
.pitch: db pitch_end

ins_ppp_03:
	dw   .vol, .arp, .duty, .pitch
	dw .vol_r,    0,     0,      0
.vol:   db 4, 12, 11, 10, 9, 8, 7, 6, 7, 12, 10, 9, 9, 8, 6, 6, 5, seq_end
.arp:   db 64+12, 0, 12, 0, seq_wait, 6, 64+5, 0, seq_end
.duty:  db 2, 1, seq_end
.pitch: db 0, 3, pitch_end
.vol_r: db 3, seq_end

ins_ppp_08:
	dw   .vol, .arp, .duty, .pitch
	dw      0,    0,     0,      0
.vol:   db 15, seq_wait, 5, 4, seq_end
.arp:   db 19,17,15,14,11,9,7,5,3,1,seq_end ; i want to do pitch down :(
.duty:  db 2, seq_end
.pitch: db pitch_end

ins_ppp_09:
	dw   .vol, .arp, .duty, .pitch
	dw      0,    0,     0,      0
.vol:   db 2,3,3,4,4,5,5,5,6,7,seq_end
.arp:   db seq_end
.duty:  db 0, seq_end
.pitch: db pitch_end

