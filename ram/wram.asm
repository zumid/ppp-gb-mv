INCLUDE "constants/constants.inc"
INCLUDE "constants/hardware.inc"

SECTION "wram", WRAM0

wVirtualOAM:: ds 4 * MAX_SPRITES

wStack:: ds $100 - 1
wStackTop:: db

wLyricIndex:: db

wScreenBuffer1:: ds SCRN_X_B * SCRN_Y_B ; to $9800
wScreenBuffer2:: ds SCRN_X_B * SCRN_Y_B ; to $9C00

SECTION "ly_overrides", WRAM0

wLYOverrides:: ds SCRN_Y
