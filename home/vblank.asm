INCLUDE "constants/hardware.inc"
INCLUDE "macros/pokered_coords.inc"

SECTION "vblank", ROM0

VBlank::
	push af
	push bc
	push de
	push hl
		xor a
		ld [hAskVBlank], a
		
; screen
		call hSpriteDMA
		call AutoBgMapTransfer
; music
		ldh a, [hHardcodedSongMeasure]
		cp 4
		jr nc, .after_the_drop
		
		call DSX_Update
		jr .keep_track_of_measure
.after_the_drop
		ldh a, [hFuckUpScreenEnabled]
		and a
		jr z, .skip_displaying_raster_time
		ld a, %00011011
		ldh [rBGP], a
.skip_displaying_raster_time
		call DSX_Update
		ld a, %11100100
		ldh [rBGP], a
.keep_track_of_measure
		ld a, [DSX_MusicPlaying]
		and a
		jr z, .skip_measure_counter
		ldh a, [hHardcodedSongMeasure]
		cp $47 ; end of song
		jr z, .skip_measure_counter
		ld a, [hHardcodedPreSongDelay]
		and a
		jr z, .measure_counter
		dec a
		ld [hHardcodedPreSongDelay], a
		jr .skip_measure_counter
.measure_counter
		ldh a, [hHardcodedSongTick]
		inc a
		cp (5 * 4 * 4) ; speed * rows
		jr nz, .dont_advance_measure_counter
		ldh a, [hHardcodedSongMeasure]
		inc a
		ldh [hHardcodedSongMeasure], a
		ld a, 0 ; preserve flag
.dont_advance_measure_counter
		ldh [hHardcodedSongTick], a
.skip_measure_counter

	pop hl
	pop de
	pop bc
	pop af
	reti

BGMAPTRANSFER_BUFFER0_TOP    equ 0
BGMAPTRANSFER_BUFFER0_BOTTOM equ 1
BGMAPTRANSFER_BUFFER1_TOP    equ 2
BGMAPTRANSFER_BUFFER1_BOTTOM equ 3
; from Pokemon Red
AutoBgMapTransfer::
; save stack pointer
	ld hl, sp + 0
	ld a, h
	ldh [hSPTemp], a
	ld a, l
	ldh [hSPTemp + 1], a

; 0 = Top half of first buffer
; 1 = Bottom half of second buffer
; 2 = Top half of second buffer
; 3 = Second half of second buffer
	ldh a, [hAutoBGTransferPortion]
	and a
	jr z, .transferTop1
	dec a
	jr z, .transferBottom1
	dec a
	jr z, .transferTop2
.transferBottom2
	hlcoord 0, 9, wScreenBuffer2
	ld sp, hl
	ld hl, _SCRN1 + (9 * SCRN_VY_B)
	xor a ; BGMAPTRANSFER_BUFFER0_TOP
	jr .init_transfer
.transferTop2
	hlcoord 0, 0, wScreenBuffer2
	ld sp, hl
	ld hl, _SCRN1 + (0 * SCRN_VY_B)
	ld a, BGMAPTRANSFER_BUFFER1_BOTTOM
	jr .init_transfer
.transferBottom1
	hlcoord 0, 9, wScreenBuffer1
	ld sp, hl
	ld hl, _SCRN0 + (9 * SCRN_VY_B)
	ld a, BGMAPTRANSFER_BUFFER1_TOP
	jr .init_transfer
.transferTop1
	hlcoord 0, 0, wScreenBuffer1
	ld sp, hl
	ld hl, _SCRN0 + (0 * SCRN_VY_B)
	ld a, BGMAPTRANSFER_BUFFER0_BOTTOM
.init_transfer
	ldh [hAutoBGTransferPortion], a ; store next portion
	ld b, 9
.do_transfer
; unrolled loop and using pop for speed
	REPT 20 / 2 - 1
	pop de
	ld [hl], e
	inc l
	ld [hl], d
	inc l
	ENDR
	pop de
	ld [hl], e
	inc l
	ld [hl], d
	ld a, 32 - (20 - 1)
	add l
	ld l, a
	jr nc, .ok
	inc h
.ok
	dec b
	jr nz, .do_transfer
	ldh a, [hSPTemp]
	ld h, a
	ldh a, [hSPTemp + 1]
	ld l, a
	ld sp, hl
	ret
