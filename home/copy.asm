INCLUDE "macros/wait_vram.inc"

SECTION "copy and fill mem functions", ROM0

;;--
;; Fill memory with HL with A continuously for
;; C bytes
;;
;; @param A    value for fill
;; @param HL   start address
;; @param C    how many bytes
;;
;; @return A   same
;; @return HL  HL + C + 1
;; @return C   0
;;--
FillMem8::
.loop
	ld [hl+], a
	dec c
	jr nz, .loop
	ret

;;--
;; Fill memory with HL continuously with A
;; for BC bytes
;;
;; @param A    value for fill
;; @param HL   start address
;; @param BC   how many bytes
;;
;; @return A   same
;; @return HL  HL + BC + 1
;; @return BC  0000
;;--
FillMem16::
	dec bc
	inc b
	inc c
.loop
	ld [hl+], a
	dec c
	jr nz, .loop
	dec b
	ret z
	jr .loop

;;--
;; Copies a portion of memory from DE to HL for
;; BC bytes.
;;
;; @param HL   destination start address
;; @param DE   source start address
;; @param BC   how many bytes
;;
;; @return A   Byte in (DE + BC)
;; @return HL  HL + BC + 1
;; @return BC  0000
;;--
CopyMem16::
	dec bc
	inc b
	inc c
.loop
	ld a, [de]
	inc de
	ld [hl+], a
	dec c
	jr nz, .loop
	dec b
	ret z
	jr .loop

;;--
;; Copies a portion of memory from DE to HL for
;; BC bytes except you can use it while the
;; screen is on
;;
;; @param HL   destination start address
;; @param DE   source start address
;; @param BC   how many bytes
;;
;; @return A   Byte in (DE + BC)
;; @return HL  HL + BC + 1
;; @return BC  0000
;;--
CopyMem16_VRAM::
	dec bc
	inc b
	inc c
.loop
	wait_vram
	ld a, [de]
	inc de
	ld [hl+], a
	dec c
	jr nz, .loop
	dec b
	ret z
	jr .loop

