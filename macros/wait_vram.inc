INCLUDE "constants/hardware.inc"

MACRO wait_vram
.waitVRAM\@
	ldh a, [rSTAT]
	and STATF_BUSY
	jr nz, .waitVRAM\@
ENDM
