INCLUDE "constants/game_mode_constants.inc"

INCLUDE "macros/pokered_coords.inc"
INCLUDE "macros/asm.inc"

SECTION "Intro game mode", ROMX

GM_Intro_INIT::
	ld hl, Song_PPP
	call DSX_PlaySong
	
	ld hl, .title
	decoord 7, 6, wScreenBuffer1
	call PutString	
	
	call ReseedScreenFuckups_Intro
	ret

.title
	db "PIPIPI@"

ReseedScreenFuckups_Intro:
	ld hl, wLYOverrides
	ld e, SCRN_Y
.keepgoing
	push hl
		call rand
		and %1111
		sub 10
.got_num
	pop hl
	ld [hli], a
	dec e
	ret z
	jr .keepgoing

GM_Intro_UPDATE::
	check_measure 2, 0
	call z, .put_author_string
	
	check_measure 1, $30
	call z, .fuckup_and_invert_palette
	
	check_measure 1, $40
	call z, .nofuckup_and_invert_palette
	
	check_measure 3, $30
	call z, .fuckup_and_invert_palette
	
	check_measure 3, $40
	call z, .nofuckup_and_invert_palette
	
	call ReseedScreenFuckups_Intro
	
	check_measure 4, 0
	ret nz
	
; switch to main mode
	ld a, GM_MAINVIZ
	ldh [hGameMode], a
	ret

.put_author_string
	ld hl, .author
	decoord 3, 8, wScreenBuffer1
	jp PutString

.fuckup_and_invert_palette
	ld a, 1
	ldh [hFuckUpScreenEnabled], a
.invert_palette
 	ldh a, [rBGP]
	cpl
	ldh [rBGP], a
	ret

.nofuckup_and_invert_palette
	xor a
	ldh [hFuckUpScreenEnabled], a
	ldh [rSCX], a
	jr .invert_palette

.author
	db "by KEN ASHCORP@"
