INCLUDE "constants/hardware.inc"

hlcoord EQUS "coord hl,"
bccoord EQUS "coord bc,"
decoord EQUS "coord de,"

MACRO coord
; register, x, y[, origin]
	ld \1, (\3) * SCRN_X_B + (\2) + \4
ENDM
