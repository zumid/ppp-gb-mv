INCLUDE "constants/game_mode_constants.inc"
INCLUDE "constants/hardware.inc"
INCLUDE "macros/pokered_coords.inc"
INCLUDE "macros/asm.inc"

SECTION "Mainviz game mode", ROMX

GM_MainViz_INIT::
; setup DevSound param display in the other screen
	ld hl, DevSoundParamTxt
	decoord 1, 1, wScreenBuffer2
	call PutString
	
	ld hl, DevSoundParamFooterTxt
	decoord 1, 16, wScreenBuffer2
	call PutString
	
; setup image on this screen
	xor a
	ld hl, wScreenBuffer1
	ld bc, SCRN_X_B * SCRN_Y_B
	call FillMem16
	call ShowImage
	ret

ShowImage:
	hlcoord 4, 2, wScreenBuffer1
	ld d, $80		; tile
	ld b, 11	; width
	ld c, 11	; height
.columnloop
	push bc
	pop bc
	ld a, d
	ld [hli], a
	inc d
	dec b
	jr nz, .columnloop
	dec c
	ret z
;fuck
	push de
		ld de, 20-11
		add hl, de
	pop de
	ld b, 11
	jr .columnloop

DevSoundParamTxt:
	db "CH1 -- / XX left`"
	db "Seq=---- Ret=----`"
	db "Vol=---- Arp=----`"
	db "Pul=---- Pch=----`"
	db " `"
	db "CH2 -- / XX left`"
	db "Seq=---- Ret=----`"
	db "Vol=---- Arp=----`"
	db "Pul=---- Pch=----`"
	db " `"
	db "CH3 -- / XX left`"
	db "Seq=---- Ret=----`"
	db "Vol=---- Arp=----`"
	db "Pul=---- Pch=----`"
	db " ` @"

DevSoundParamFooterTxt:
	db "  SOUND MONITOR  @"

GM_MainViz_UPDATE::
	check_measure 8, 0
	call z, .SwitchToScreen2
	
	check_measure $14, 0
	call z, .SwitchToScreen1
	
	check_measure $24, 0
	call z, .SwitchToScreen2
	
	check_measure $25, $30
	call z, .DisplayXText
	
	check_measure $25, $40
	call z, .DisplayNormalText
	
	check_measure $2C, 0
	call z, .SwitchToScreen1
	
	check_measure $42, 0
	call z, .SwitchToScreen2
	
	check_measure $47, 0
	call z, .stop
	
	call GM_MainViz_ShowLyrics
	
	ldh a, [hHardcodedSongMeasure]
	cp $44
	jr nc, .more_fuck_ups

; little fuck ups
	ld b, 45
	call .trigger_fuck_ups
	jr .after_fuck_ups

.more_fuck_ups
	call ReseedScreenFuckups
	ld b, 254
	call .trigger_fuck_ups

.after_fuck_ups
	call UpdateDevSoundParamDisplay
	ret

.stop
	call DelayFrame
	call DelayFrame
	call DelayFrame
	call DelayFrame
	call DelayFrame
	call DelayFrame
	call DelayFrame
	ld a, $ff
	ldh [rBGP], a
	stop

.trigger_fuck_ups
	push bc
		call rand
	pop bc
	cp b
	jr c, .enable
	xor a
	ldh [hFuckUpScreenEnabled], a
	ldh [rSCX], a
	ret
.enable
	ld a, 1
	ldh [hFuckUpScreenEnabled], a
	ret

.SwitchToScreen2
	ldh a, [rLCDC]
	set LCDCB_BG9C00, a
	ldh [rLCDC], a
	ret

.SwitchToScreen1
	ldh a, [rLCDC]
	res LCDCB_BG9C00, a
	ldh [rLCDC], a
	ret

.DisplayXText
	ld hl, .textx
	decoord 1, 16, wScreenBuffer2
	call PutString
	ret

.textx
	db " THE MERGE IS ON!@"

.DisplayNormalText
	ld hl, DevSoundParamFooterTxt
	decoord 1, 16, wScreenBuffer2
	call PutString
	ret

GM_MainViz_ShowLyrics:
; get entry
	ld a, [wLyricIndex]
	sla a ; a * 4
	sla a
	ld e, a
	ld d, 0
	ld hl, Lyrics_Table
	add hl, de
	ld a, [hli]
	cp -1
	ret z ; quit if end
	ld b, a
	ld c, [hl]
	call CheckMeasure
	ret nz ; quit if we're not here yet
; clear text
	push hl
		xor a
		hlcoord 0, 13, wScreenBuffer1
		ld bc, SCRN_X_B * 5
		call FillMem16
	pop hl
; display text
	inc hl
	ld a, [hli]
	ld h, [hl]
	ld l, a
	or h
	jr z, .increment_index ; if text == 0
	decoord 1, 14, wScreenBuffer1
	call PutString
.increment_index
	ld a, [wLyricIndex]
	inc a
	ld [wLyricIndex], a
	call ReseedScreenFuckups
	ret

MACRO lyric
	db \1 ; measure
	db \2 ; tick
	dw \3 ; lyric text (0 = clear)
ENDM

Lyrics_Table:
	lyric $14, $22, .text_1
	lyric $16, $12, .text_2
	lyric $18, $00, 0
	lyric $18, $1c, .text_3
	lyric $19, $44, .text_4
	lyric $1c, $00, 0
	lyric $1c, $20, .text_5
	lyric $1e, $18, .text_6
	lyric $20, $00, 0
	lyric $20, $1c, .text_7
	lyric $24, $00, 0
	lyric $2c, $00, .text_8
	lyric $2d, $37, .text_9
	lyric $2f, $20, .text_10
	lyric $31, $26, .text_11
	lyric $33, $26, 0
	lyric $33, $4e, .text_12
	lyric $35, $43, .text_13
	lyric $37, $21, .text_14
	lyric $39, $23, .text_15
	lyric $3a, $30, 0
	lyric $3b, $45, .text_16
	lyric $3d, $22, .text_17
	lyric $40, $00, 0
	db -1

.text_1
	db "    Tune in to`     tune out@"

.text_2
	db "   It's too late`    to stop now@"

.text_3
	db "    Flip those`  burgers, wagie@"

.text_4
	db "    Just enjoy`     the show@"

.text_5
	db "  Godless heathen@"

.text_6
	db "    Fatherless`   and seething@"

.text_7
	db "  Take me to your`  power cord and`  watch me glow@"

.text_8
	db "  Susan's on the`   Franzia and@"

.text_9
	db "    Jeff is on`     the coke@"

.text_10
	db "  I am inside of`    your walls`    right now@"

.text_11
	db "     You can't`     escape me@"

.text_12
	db "  Take your pills`    and keep on`     breathing@"

.text_13
	db "  This is not`  a joke@"

.text_14
	db "Parasocial paranoia@"

.text_15
	db "   I just wanna`     chill out@"

.text_16
	db "     Put your` tin foil hat on@"

.text_17
	db "I am just a stupid`   bunny rabbit@"

ReseedScreenFuckups:
	ld hl, wLYOverrides
	ld e, SCRN_Y
.keepgoing
	push hl
		call rand
		cp 98
		jr nc, .set_zero
		and %111
		
		jr .got_num
.set_zero
		ld a, 0
.got_num
	pop hl
	ld [hli], a
	dec e
	ret z
	jr .keepgoing

UpdateDevSoundParamDisplay:
	ldh a, [hHardcodedSongTick]
	cp 10
	call c, ReseedScreenFuckups
	cp 40
	call z, ReseedScreenFuckups
	
; notes
	ld de, DSX_CH1_Note
	hlcoord 5, 1, wScreenBuffer2
	call Byte2Display
	
	ld de, DSX_CH2_Note
	hlcoord 5, 6, wScreenBuffer2
	call Byte2Display
	
	ld de, DSX_CH3_Note
	hlcoord 5, 11, wScreenBuffer2
	call Byte2Display

; timer
	ld de, DSX_CH1_Timer
	hlcoord 10, 1, wScreenBuffer2
	call Byte2Display
	
	ld de, DSX_CH2_Timer
	hlcoord 10, 6, wScreenBuffer2
	call Byte2Display
	
	ld de, DSX_CH3_Timer
	hlcoord 10, 11, wScreenBuffer2
	call Byte2Display

; slide offset
	ld de, DSX_CH1_SlideOffset
	hlcoord 14, 4, wScreenBuffer2
	call Word2Display
	
	ld de, DSX_CH2_SlideOffset
	hlcoord 14, 9, wScreenBuffer2
	call Word2Display
	
	ld de, DSX_CH3_SlideOffset
	hlcoord 14, 14, wScreenBuffer2
	call Word2Display

; sequence
	ld de, DSX_CH1_SeqPtr
	hlcoord 5, 2, wScreenBuffer2
	call Word2Display
	
	ld de, DSX_CH2_SeqPtr
	hlcoord 5, 7, wScreenBuffer2
	call Word2Display
	
	ld de, DSX_CH3_SeqPtr
	hlcoord 5, 12, wScreenBuffer2
	call Word2Display

; volume
	ld de, DSX_CH1_VolPtr
	hlcoord 5, 3, wScreenBuffer2
	call Word2Display
	
	ld de, DSX_CH2_VolPtr
	hlcoord 5, 8, wScreenBuffer2
	call Word2Display
	
	ld de, DSX_CH3_VolPtr
	hlcoord 5, 13, wScreenBuffer2
	call Word2Display

; pulse
	ld de, DSX_CH1_PulsePtr
	hlcoord 5, 4, wScreenBuffer2
	call Word2Display
	
	ld de, DSX_CH2_PulsePtr
	hlcoord 5, 9, wScreenBuffer2
	call Word2Display
	
	ld de, DSX_CH3_PulsePtr
	hlcoord 5, 14, wScreenBuffer2
	call Word2Display

; arp
	ld de, DSX_CH1_ArpPtr
	hlcoord 14, 3, wScreenBuffer2
	call Word2Display
	
	ld de, DSX_CH2_ArpPtr
	hlcoord 14, 8, wScreenBuffer2
	call Word2Display
	
	ld de, DSX_CH3_ArpPtr
	hlcoord 14, 13, wScreenBuffer2
	call Word2Display

; return
	ld de, DSX_CH1_ReturnPtr
	hlcoord 14, 2, wScreenBuffer2
	call Word2Display
	
	ld de, DSX_CH2_ReturnPtr
	hlcoord 14, 7, wScreenBuffer2
	call Word2Display
	
	ld de, DSX_CH3_ReturnPtr
	hlcoord 14, 12, wScreenBuffer2
	call Word2Display
	ret

Word2Display:
; hl = print address
; de = pointer to byte
	inc de
	call Byte2Display
	dec de
Byte2Display:
; hl = print address
; de = pointer to byte
	ld a, [de]		; fetch byte
; get high nibble
	and a, %11110000
	swap a
	call .determinenum	; print 1st number
; get lower nibble
	ld a, [de]
	and a, %00001111
	call .determinenum	; print 2nd number
	ret
.determinenum
	cp 10
	jr nc, .hex		; if byte >= $A
	add "0"			; decimal numbers
	jr .print
.hex
	sub 9
	add "@"			; hex
.print
	ld b, a
	ld a, b
	ld [hli], a
	ret
