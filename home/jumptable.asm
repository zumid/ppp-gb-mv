SECTION "jumptable funcs", ROM0
;;--
;; Go to jumptable entry
;; 
;; @param HL  Jumptable address containing pointers
;; @param A   Entry number
;; @clobber DE
;;--
GotoJumptableEntry::
	ld e, a
	ld d, 0
	add hl, de
	add hl, de
	ld a, [hli]
	ld h, [hl]
	ld l, a
	jp hl
