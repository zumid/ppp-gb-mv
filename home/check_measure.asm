SECTION "check measure func", ROM0

;;--
;; Check for exact measure and tick
;;
;; @param B    Measure
;; @param C    Tick
;;
;; @return Z parameters match
;;--
CheckMeasure::
	ldh a, [hHardcodedSongMeasure]
	cp b
	ret nz
	ldh a, [hHardcodedSongTick]
	cp c
	ret
