; ================================================================
; DevSound X sound driver for Game Boy
; Copyright (c) 2022 DevEd
; 
; Permission is hereby granted, free of charge, to any person obtaining
; a copy of this software and associated documentation files (the
; "Software"), to deal in the Software without restriction, including
; without limitation the rights to use, copy, modify, merge, publish,
; distribute, sublicense, and/or sell copies of the Software, and to
; permit persons to whom the Software is furnished to do so, subject to
; the following conditions:
; 
; The above copyright notice and this permission notice shall be included
; in all copies or substantial portions of the Software.
; 
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
; IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
; CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
; TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
; SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
; ================================================================

macro DSX_ChannelStruct
DSX_CH\1_Start:
DSX_CH\1_SeqPtr:            dw
DSX_CH\1_ReturnPtr:         dw
DSX_CH\1_VolPtr:            dw
DSX_CH\1_ArpPtr:            dw
DSX_CH\1_NoisePtr:
DSX_CH\1_WavePtr:   
DSX_CH\1_PulsePtr:          dw
if \1 != 4
DSX_CH\1_PitchPtr:          dw
endc
DSX_CH\1_LoopCount:         db
DSX_CH\1_VolDelay:          db
DSX_CH\1_ArpDelay:          db
DSX_CH\1_NoiseDelay:
DSX_CH\1_WaveDelay:
DSX_CH\1_PulseDelay:        db
if \1 != 4
DSX_CH\1_PitchDelay:        db
endc
DSX_CH\1_Tick:              db
DSX_CH\1_Note:              db
DSX_CH\1_Timer:             db
DSX_CH\1_ArpTranspose:      db
if \1 != 4
DSX_CH\1_PitchMode:         db  ; 0 = normal, 1 = slide up, 2 = slide down, 3 = portamento, bit 7 = monty flag
DSX_CH\1_VibOffset:         dw
DSX_CH\1_SlideOffset:       dw
DSX_CH\1_SlideTarget:       dw
DSX_CH\1_SlideSpeed:        db
DSX_CH\1_NoteTarget:        db
DSX_CH\1_Transpose:         db
endc
DSX_CH\1_VolResetPtr:       dw
DSX_CH\1_VolReleasePtr:     dw
DSX_CH\1_ArpResetPtr:       dw
DSX_CH\1_ArpReleasePtr:     dw
DSX_CH\1_WaveResetPtr:
DSX_CH\1_PulseResetPtr:     dw
DSX_CH\1_WaveReleasePtr:
DSX_CH\1_PulseReleasePtr:   dw
if \1 != 4
DSX_CH\1_PitchResetPtr:     dw
DSX_CH\1_PitchReleasePtr:   dw
endc
if \1 == 3
DSX_CH\1_SamplePointer:     dw
DSX_CH\1_SampleLength:      dw
endc
if \1 == 3
DSX_CH\1_NRX0:              db
else
DSX_CH\1_ChannelVol:        db
endc
DSX_CH\1_EchoPos:           db
DSX_CH\1_FirstNote:         db
DSX_CH\1_CurrentWave:
DSX_CH\1_NRX1:              db
DSX_CH\1_CurrentNRX2:       db
DSX_CH\1_PreviousWave:
DSX_CH\1_PreviousNRX2:      db
DSX_CH\1_NRX3:              db
DSX_CH\1_NRX4:              db
DSX_CH\1_End:
endm

; PARAMETERS: [length]
macro rest
    db      $7f,\1
    endm

macro wait
    db      $7e,\1
    endm

macro release
    db      $7d,\1
    endm

; PARAMETERS: [note, octave, length]
macro note
    db      \1 + (12 * (\2 - 2)), \3
    endm
    
; PARAMETERS: [length]
macro sfix
    note    C_,2,\1
    endm

; PARAMETERS: [instrument, length]
macro sfixins
    sound_instrument \1
    note    C_,2,\2
    endm

; Specifies instrument to use for current channel (Any channel)
; PARAMETERS: [pointer to instrument data]
macro sound_instrument
    db      $80
    dw      \1
    endm

; Jump to another section of sound data (Any channel)
; PARAMETERS: [pointer to jump to]
macro sound_jump
    db      $81
    dw      \1
    endm

; Loop a block of sound data a specified number of times (Any channel)
; PARAMETERS: [number of loops], [return pointer]
macro sound_loop
    db      $82
    db      \1
    dw      \2
    endm

; Call a subsection of sound data (Any channel)
; PARAMETERS: [pointer of section to call]
; WARNING: Subsections cannot be nested!
macro sound_call
    db      $83
    dw      \1
    endm

; Return from a called sound data section (Any channel)
; PARAMETERS: none
; WARNING: Do not use outside of a subsection!
macro sound_ret
    db      $84
    endm

; Slide up (CH1, CH2, CH3)
; PARAMETERS: [speed]
macro sound_slide_up
    db      $85
    db      \1
    endm

; Slide down (CH1, CH2, CH3)
; PARAMETERS: [speed]
macro sound_slide_down
    db      $86
    db      \1
    endm

; Slide to a given note (CH1, CH2, CH3)
; PARAMETERS: [speed]
macro sound_portamento
    db      $87
    db      \1
    endm

; Toggle "monty mode" (only applies pitch slides on even ticks) (CH1, CH2, CH3)
; PARAMETERS: none
macro sound_toggle_monty
    db      $88
    endm

; Play a 1920hz sample (CH3 only)
; PARAMETERS: [pointer, length]
macro sound_sample
    db      $89
    dw      \1
    db      \2
    endm

; Set the volume for the current channel (CH1, CH2, CH4)
; PARAMETERS: [volume (0 - F)]
macro sound_volume
    db      $8a
    db      \1
    endm

; Transpose the CURRENT channel up or down. (CH1, CH2, CH3)
; PARAMETERS: [number of semitones to transpose by]
macro sound_transpose
	db		$8b
	db		\1
	endm

; Transpose ALL channels up or down. (Any channel)
; PARAMETERS: [number of semitones to transpose by]
; NOTE: Can be used on CH4, but does not affect it.
; WARNING: Will override any existing per-channel transposition!
macro sound_transpose_global
	db		$8c
	db		\1
	endm

; Reset the transposition for the CURRENT channel. (CH1, CH2, CH3)
; PARAMETERS: [none]
macro sound_reset_transpose
	db		$8d
	endm

; Reset the transposition for ALL channels. (Any channel)
; PARAMETERS: [none]
; NOTE: Can be used on CH4, but does not affect it.
macro sound_reset_transpose_global
	db		$8e
	endm

; Set the arpeggio pointer. (Any channel)
; PARAMETERS: [pointer]
macro sound_set_arp_ptr
    db      $8f
    dw      \1
    endm

; Set the song speed. (Any channel)
; PARAMETERS [speed 1, speed 2]
macro sound_set_speed
    db      $90
    db      \1,\2
    endm

; Marks the end of the sound data for the current channel. (Any channel)
; PARAMETERS: none
macro sound_end
    db      $ff
endm
