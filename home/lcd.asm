INCLUDE "constants/hardware.inc"

SECTION "lcd functions", ROM0

;;--
;; Turns off the screen
;;
;; @return [rLCDC] ~= LCD_ENABLE_BIT
;;--
DisableLCD::
.wait
	ldh a, [rLY]
	cp SCRN_Y
	jr nc, .disable ; >= LY_VBLANK? jump if yes
	jr .wait        ; otherwise keep waiting
.disable
	ldh a, [rLCDC]
	res LCDCB_ON, a ; disable LCD
	ldh [rLCDC], a
	ret
