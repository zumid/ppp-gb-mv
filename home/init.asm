INCLUDE "constants/hardware.inc"
INCLUDE "constants/constants.inc"

SECTION "entry point", ROM0
	jp Start

SECTION "init", ROM0
Start::
	di
	ldh [hGBType], a
	call DisableLCD

; clr WRAM
	xor a
	ld hl, $c000 ; absolute wram start
	ld bc, $2000
	call FillMem16
	ld sp, wStackTop

; clr HRAM
	ld hl, hGBType + 1
	ld c, $ffff - (hGBType-1)
	call FillMem8

; clr VRAM
	ld hl, $8000
	ld bc, $2000
	call FillMem16

; init audio registers
	ldh [rNR52], a
	
; init video registers
	ldh [rSCX], a
	ldh [rSCY], a
	ld a, SCRN_Y
	ldh [rWY], a
	ld a, 7
	ldh [rWX], a
	ld a, %11100100
	ldh [rBGP], a  ; background
	ldh [rOBP0], a ; object palette 0
	ldh [rOBP1], a ; object palette 1

; copy DMA code
	ld hl, SpriteDMA
	ld b, hSpriteDMA.end - hSpriteDMA
	ld c, LOW(hSpriteDMA)
.copy_code
	ld a, [hli]
	ld [c], a
	inc c
	dec b
	jr nz, .copy_code

; init graphics
	ld de, GBSMonoFont
	ld hl, $8200
	ld bc, GBSMonoFont.End - GBSMonoFont
	call CopyMem16
	
	ld de, PipipiCover
	ld hl, $8800
	ld bc, PipipiCover.End - PipipiCover
	call CopyMem16

; turn on LCD
	ld a, LCDCF_ON | LCDCF_BLK01 | \
	      LCDCF_WIN9800 | LCDCF_OBJ8 | \
	      LCDCF_OBJON | LCDCF_BGON
	ldh [rLCDC], a
	
	ld a, STATF_MODE00
	ldh [rSTAT], a
	
	ld a, IEF_VBLANK | IEF_STAT
	ldh [rIE], a
	
	ld a, 16
	ldh [hHardcodedPreSongDelay], a
	
	call DSX_Init
	
	ld a, -1
	ldh [hOldGameMode], a
	
	ei

MainLoop:
	ldh a, [hOldGameMode]
	ld b, a
	ldh a, [hGameMode]
	cp b
	jr z, .skip_init

	ld hl, Jumptable_InitGM
	call GotoJumptableEntry
	
	ldh a, [hGameMode]
	ldh [hOldGameMode], a

.skip_init
	ld hl, Jumptable_UpdateGM
	call GotoJumptableEntry
	
	call DelayFrame
	jr MainLoop

DelayFrame:
	ld a, 1
	ldh [hAskVBlank], a
.delay
	halt
	nop
	ldh a, [hAskVBlank]
	and a
	ret z
	jr .delay
	

SpriteDMA:
LOAD "sprite DMA", HRAM
hSpriteDMA::
	ld a, HIGH(wVirtualOAM)
	ldh [rDMA], a
	ld a, MAX_SPRITES
.wait
	dec a
	jr nz, .wait
	ret
.end
