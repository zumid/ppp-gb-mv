INCLUDE "constants/hardware.inc"

SECTION "hblank", ROM0

HBlank::
	push af
	ldh a, [hFuckUpScreenEnabled]
	and a
	jr z, .done
	ld a, LOW(rSCX)
	push bc
	ldh a, [rLY]
	ld c, a
	ld b, HIGH(wLYOverrides)
	ld a, [bc]
	ld b, a
	ld c, LOW(rSCX)
	ld a, b
	ldh [c], a
	pop bc
.done
	pop af
	reti
