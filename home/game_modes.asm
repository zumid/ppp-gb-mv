SECTION "Game mode jumptable", ROM0

Jumptable_InitGM::
	dw GM_Intro_INIT
	dw GM_MainViz_INIT

Jumptable_UpdateGM::
	dw GM_Intro_UPDATE
	dw GM_MainViz_UPDATE
