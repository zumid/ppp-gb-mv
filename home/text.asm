INCLUDE "constants/hardware.inc"

SECTION "text functions", ROM0

;;--
;; Prints a string pointed by HL at DE.
;; Special commands are: @ (terminate) and ` (new line)
;; 
;; @param	hl	Source
;; @param	de	Dest
;;--
PutString::
	push de
.loop
		ld a, [hli]
		cp "@"
		jr z, .done ; @ = string terminator
		cp "`"
		jr nz, .no_line
; ` = move down 1 line
		ld a, [hli]
	pop de
	push hl
		ld hl, SCRN_X_B
		add hl, de
		ld d, h
		ld e, l
	pop hl
	push de
.no_line
		ld [de], a
		inc de
		jr .loop
.done
	pop de
	ret
